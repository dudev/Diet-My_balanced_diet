<?php

namespace app\models\api\search;

use general\controllers\api\Controller;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;
use yii\data\Pagination;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'copy_from', 'category_id'], 'integer'],
            ['name', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @param int $page
	 * @return ActiveDataProvider
	 */
    public function search($params, $page = 0) {
	    $query = Product::find()
		    ->select(['id', 'name', 'description', 'owner_id', 'category_id', 'copy_from'])
	        ->with('category');

	    // load the search form data and validate
	    if ($this->load($params) && $this->validate()) {
		    $query->andFilterWhere([
			    'owner_id' => $this->owner_id,
			    'copy_from' => $this->copy_from,
			    'category_id' => $this->category_id,
		    ]);

		    $query->andFilterWhere(['like', 'name', $this->name]);
	    }

	    $count_query = clone $query;
	    $total_count = $count_query->limit(-1)->offset(-1)->orderBy([])->count();
	    if($total_count == 0) {
		    return Controller::ERROR_NO_PRODUCT;
	    }
	    $pages = new Pagination(['totalCount' => $total_count]);
	    $pages->setPage($page - 1);
	    $pages->setPageSize(30);

	    $query->offset($pages->offset)
		    ->limit($pages->limit);

	    if ($sort = \Yii::$app->request->get('sort')) {
		    if(strncmp($sort, '-', 1) === 0) {
			    $query->orderBy([substr($sort, 1) => SORT_DESC]);
		    } else {
			    $query->orderBy([$sort => SORT_ASC]);
		    }
	    }

	    $products = [];
	    foreach ($query->all() as $val) {
		    /* @var $val Product */
		    $products[] = array_merge(
			    $val->getAttributes(['id', 'name', 'description', 'owner_id', 'copy_from']),
			    [
				    'category' => empty($val->category) ? [] : $val->category->getAttributes(['id', 'name'])
			    ]
		    );
	    }

	    return [
		    'products' => $products,
		    'totalCount' => $total_count,
		    'page' => $pages->getPage()
	    ];
    }
}
