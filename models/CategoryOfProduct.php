<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "category_of_product".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $number
 *
 * @property CategoryOfProduct $parent
 * @property CategoryOfProduct[] $categoryOfProducts
 * @property Product[] $products
 */
class CategoryOfProduct extends ActiveRecord {
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_of_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            [['parent_id', 'number'], 'integer'],
	        [
		        'parent_id',
		        'exist',
		        'targetClass' => CategoryOfProduct::className(),
		        'targetAttribute' => 'id',
		        'when' => function($model) {
			        /* @var $model CategoryOfProduct */
			        return $model->parent_id != 0;
		        }
	        ],
	        ['parent_id', 'filter', 'filter' => function($value) {
		        if($value == 0) {
			        return null;
		        }
		        return $value;
	        }],
            [['name'], 'string', 'max' => 50],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Категория',
            'parent_id' => 'ID родителя',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(CategoryOfProduct::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryOfProducts()
    {
        return $this->hasMany(CategoryOfProduct::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

	public static function getMenu() {
		$category_of_product = [];
		if($res = CategoryOfProduct::find()->all()) {
			$categories = [];
			foreach ($res as $cat) {
				/* @var $cat CategoryOfArticle */
				if(is_null($cat->parent_id)) {
					$categories[0][] = $cat;
				} else {
					$categories[ $cat->parent_id ][] = $cat;
				}
			}
			if(isset($categories[0])) {
				$category_of_product = CategoryOfProduct::genMenu($categories[0], $categories);
			}
		}
		return $category_of_product;
	}

	public static function genMenu($cats, &$categories) {
		$parents = [];
		foreach ($cats as $cat) {
			$parents[] = [
				'label' => $cat['name'],
				'url' => ['product/index', 'category' => $cat['id']],
				'items' => isset($categories[ $cat['id'] ]) ? self::genMenu($categories[ $cat['id'] ], $categories) : [],
			];
		}
		return $parents;
	}
}
