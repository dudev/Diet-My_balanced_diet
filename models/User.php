<?php

namespace app\models;

use general\ext\api\auth\AuthApi;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Command;
use yii\db\Query;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {
	private $_auth_key;
	private static $_identities = [];

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules() {
        return [
	        ['nick', 'string', 'min' => 2, 'max' => 50],
        ];
    }

    public static function findIdentity($id) {
	    if(array_key_exists($id, static::$_identities)) {
		    return static::$_identities[ $id ];
	    }
	    return static::$_identities[ $id ] = static::findOne(['id' => $id]);
    }

	public static function findIdentityByAccessToken($token, $type = null) {
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

    public static function logout() {
	    (new Command(['db' => Yii::$app->db]))
		    ->delete(AuthKey::tableName(), [
			    'user_id' => Yii::$app->user->id,
			    'ip' => Yii::$app->request->getUserIP(),
			    'browser' => Yii::$app->request->getUserAgent(),
		    ])
		    ->execute();
	    Yii::$app->user->logout();
    }

	public static function logoutEverywhere() {
		(new Command(['db' => Yii::$app->db]))
			->delete(AuthKey::tableName(), [
				'user_id' => Yii::$app->user->id,
			])
			->execute();
		Yii::$app->user->logout();
	}
    public function getId() {
        return $this->id;
    }

	public static function getFromAuth($id) {
		if($model = User::findOne($id)) {
			return $model;
		}

		if(!$user = AuthApi::userSearch(['id' => $id])) {
			return null;
		}

		$model = new User();
		$model->id = $user['users'][0]['id'];
		$model->nick = $user['users'][0]['login']; //@todo возможно стоит ником брать имя-фамилию
		if($model->save()) {
			return $model;
		}
		return null;
	}

    public function getAuthKey() {
	    if($this->_auth_key) {
		    return $this->_auth_key;
	    }
	    $res = (new Query())
		    ->select('auth_key')
		    ->from(AuthKey::tableName())
		    ->where([
			    'user_id' => $this->id,
			    'ip' => Yii::$app->request->getUserIP(),
			    'browser' => Yii::$app->request->getUserAgent(),
		    ])
	        ->one();
	    if($res) {
		    return $this->_auth_key = $res['auth_key'];
	    }
	    return false;
    }

    public function validateAuthKey($authKey) {
	    if($auth_key = $this->getAuthKey()) {
		    return $auth_key === $authKey;
	    }
	    return false;
    }
}
