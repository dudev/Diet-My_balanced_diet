<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\imagine\Image;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property double $protein
 * @property double $fat
 * @property double $carbohydrate
 * @property double $calorie
 * @property double $dietary_fiber
 * @property double $cholesterol
 * @property double $water
 * @property double $ca
 * @property double $fe
 * @property double $na
 * @property double $k
 * @property double $mg
 * @property double $p
 * @property double $co
 * @property double $cr
 * @property double $cu
 * @property double $mn
 * @property double $s
 * @property double $si
 * @property double $v
 * @property double $zn
 * @property double $mo
 * @property double $f
 * @property double $i
 * @property double $se
 * @property double $b
 * @property double $cl
 * @property double $a
 * @property double $b_car
 * @property double $b1
 * @property double $b2
 * @property double $b4
 * @property double $b5
 * @property double $b6
 * @property double $b7
 * @property double $b9
 * @property double $b12
 * @property double $c
 * @property double $d
 * @property double $e
 * @property double $pp
 * @property double $k_phylloquinone
 * @property double $tryptophan
 * @property double $lysine
 * @property double $methionine
 * @property double $threonine
 * @property double $leucine
 * @property double $isoleucine
 * @property double $phenylalanine
 * @property double $valine
 * @property integer $owner_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $copy_from
 * @property integer $category_id
 *
 * @property CategoryOfProduct $category
 * @property Product $copyFrom
 * @property Product[] $products
 * @property User $owner
 *
 * @property \yii\web\UploadedFile $image
 */
class Product extends ActiveRecord {
	public $image;

	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
	        ['name', 'string', 'max' => 50],
            ['description', 'string'],
	        ['description', 'filter', 'filter' => function($value) {
		        if(empty($value)) {
			        return null;
		        }
		        return $value;
	        }],
            [['protein', 'fat', 'carbohydrate', 'calorie', 'dietary_fiber', 'cholesterol', 'water', 'ca', 'fe', 'na', 'k', 'mg', 'p', 'co', 'cr', 'cu', 'mn', 's', 'si', 'v', 'zn', 'mo', 'f', 'i', 'se', 'b', 'cl', 'a', 'b_car', 'b1', 'b2', 'b4', 'b5', 'b6', 'b7', 'b9', 'b12', 'c', 'd', 'e', 'pp', 'k_phylloquinone', 'tryptophan', 'lysine', 'methionine', 'threonine', 'leucine', 'isoleucine', 'phenylalanine', 'valine'], 'number'],
	        [
		        'owner_id',
		        'exist',
		        'targetClass' => User::className(),
		        'targetAttribute' => 'id',
		        'when' => function($model) {
			        /* @var $model Product */
			        return $model->owner_id != 0;
		        }
	        ],
	        ['owner_id', 'filter', 'filter' => function($value) {
		        if(empty($value) || $value == 0) {
			        return null;
		        }
		        return $value;
	        }],
	        [
		        'copy_from',
		        'exist',
		        'targetClass' => Product::className(),
		        'targetAttribute' => 'id',
		        'when' => function($model) {
			        /* @var $model Product */
			        return $model->copy_from != 0;
		        }
	        ],
	        ['copy_from', 'filter', 'filter' => function($value) {
		        if(empty($value) || $value == 0) {
			        return null;
		        }
		        return $value;
	        }],
	        [
		        'category_id',
		        'exist',
		        'targetClass' => CategoryOfProduct::className(),
		        'targetAttribute' => 'id',
		        'when' => function($model) {
			        /* @var $model Product */
			        return $model->category_id != 0;
		        }
	        ],
	        ['category_id', 'filter', 'filter' => function($value) {
		        if(empty($value) || $value == 0) {
			        return null;
		        }
		        return $value;
	        }],
	        [
		        'image',
		        'file',
		        'skipOnEmpty' => false,
		        'when' => function($model) {
			        /* @var $model Product */
			        return $model->isNewRecord || !file_exists(\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '.jpg');
		        },
		        'whenClient' => 'function (attribute, value) {
			        return ' . ($this->isNewRecord ? 'true' : 'false') . ' || ' . (!file_exists(\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '.jpg') ? 'true' : 'false') . ';
			    }',
		        'uploadRequired' => 'Необходимо прикрепить картинку.',
	        ],
	        [
		        'image',
		        'file',
		        'extensions' => 'jpg, jpeg, gif',
		        'mimeTypes' => 'image/jpeg, image/gif',
	        ],
        ];
    }

	public function saveImage() {
		if($this->image) {
			if(file_exists(\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '.jpg')) {
				unlink(\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '.jpg');
			}
			if(file_exists(\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '-big.jpg')) {
				unlink(\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '-big.jpg');
			}
			if(file_exists(\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '-small.jpg')) {
				unlink(\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '-small.jpg');
			}

			$this->image->saveAs(\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '.jpg');
			//формируем большую картинку
			Image::thumbnail(
				\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '.jpg',
				640,
				640
			)->save(
				\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '-big.jpg',
				['quality' => 90]
			);
			//формируем маленькую картинку
			Image::thumbnail(
				\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '-big.jpg',
				320,
				320
			)->save(
				\Yii::getAlias('@app/web/i/product') . '/' . $this->id . '-small.jpg',
				['quality' => 70]
			);
		}
	}

	public static function getImageLinks($model, $types = ['source', 'big', 'small']) {
		$ret = [];
		if(file_exists(\Yii::getAlias('@app/web/i/product') . '/' . $model['id'] . '.jpg')) {
			foreach ((array)$types as $type) {
				$ret[$type] = '//' . Yii::$app->request->serverName . '/i/product/' . $model['id'];
				if ($type == 'source') {
					$ret[$type] .= '.jpg';
				} else {
					$ret[$type] .= '-' . $type . '.jpg';
				}
			}
		}
		return $ret;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'protein' => 'Белки',
            'fat' => 'Жиры',
            'carbohydrate' => 'Углеводы',
            'calorie' => 'Калорийность',
            'dietary_fiber' => 'Пищевые волокна',
            'cholesterol' => 'Холестерин',
            'water' => 'Процент воды',
            'ca' => 'Кальций. Ca, мг',
            'fe' => 'Железо. Fe, мг',
            'na' => 'Натрий. Na, мг',
            'k' => 'Калий. K, мг',
            'mg' => 'Магний. Mg, мг',
            'p' => 'P, мг',
            'co' => 'Co, мг',
            'cr' => 'Cr, мг',
            'cu' => 'Cu, мг',
            'mn' => 'Mn, мг',
            's' => 'S, мг',
            'si' => 'Si, мг',
            'v' => 'V, мг',
            'zn' => 'Zn, мг',
            'mo' => 'Mo, мг',
            'f' => 'F, мг',
            'i' => 'I, мг',
            'se' => 'Se, мг',
            'b' => 'B, мг',
            'cl' => 'Cl, мг',
            'a' => 'A, мг',
            'b_car' => 'B-car, мг',
            'b1' => 'B1, мг',
            'b2' => 'B2, мг',
            'b4' => 'B4, мг',
            'b5' => 'B5, мг',
            'b6' => 'B6, мг',
            'b7' => 'B7, мг',
            'b9' => 'B9, мг',
            'b12' => 'B12, мг',
            'c' => 'C, мг',
            'd' => 'D, мг',
            'e' => 'E, мг',
            'pp' => 'PP, мг',
            'k_phylloquinone' => 'K (филлохинон), мг',
            'tryptophan' => 'Триптофан, мг',
            'lysine' => 'Лизин, мг',
            'methionine' => 'Метионин, мг',
            'threonine' => 'Треонин, мг',
            'leucine' => 'Лейцин, мг',
            'isoleucine' => 'Изолейцин, мг',
            'phenylalanine' => 'Фенилаланин, мг',
            'valine' => 'Валин, мг',
            'owner_id' => 'Владелец',
            'copy_from' => 'Копия от',
            'category_id' => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CategoryOfProduct::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCopyFrom()
    {
        return $this->hasOne(Product::className(), ['id' => 'copy_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['copy_from' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
}
