<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "category_of_article".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $number
 *
 * @property CategoryOfArticle $parent
 * @property CategoryOfArticle[] $categoryOfArticles
 */
class CategoryOfArticle extends ActiveRecord {
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'category_of_article';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'number'], 'required'],
            ['parent_id', 'integer'],
	        [
		        'parent_id',
		        'exist',
		        'targetClass' => CategoryOfArticle::className(),
		        'targetAttribute' => 'id',
		        'when' => function($model) {
			        /* @var $model CategoryOfArticle */
			        return $model->parent_id != 0;
		        }
	        ],
	        ['parent_id', 'filter', 'filter' => function($value) {
		        if($value == 0) {
			        return null;
		        }
		        return $value;
	        }],
	        ['number', 'integer'],
            ['name', 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Категория',
            'parent_id' => 'ID родителя',
	        'number' => 'Номер',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(CategoryOfArticle::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryOfArticles() {
        return $this->hasMany(CategoryOfArticle::className(), ['parent_id' => 'id']);
    }

	public static function getMenu() {
		$category_of_article = [];
		if($res = CategoryOfArticle::find()->all()) {
			$categories = [];
			foreach ($res as $cat) {
				/* @var $cat CategoryOfArticle */
				if(is_null($cat->parent_id)) {
					$categories[0][] = $cat;
				} else {
					$categories[ $cat->parent_id ][] = $cat;
				}
			}
			if(isset($categories[0])) {
				$category_of_article = CategoryOfArticle::genMenu($categories[0], $categories);
			}
		}
		return $category_of_article;
	}

	public static function genMenu($cats, &$categories) {
		$parents = [];
		foreach ($cats as $cat) {
			$parents[] = [
				'label' => $cat['name'],
				'url' => ['article/list', 'tag' => $cat['name']],
				'items' => isset($categories[ $cat['id'] ]) ? self::genMenu($categories[ $cat['id'] ], $categories) : [],
			];
		}
		return $parents;
	}
}
