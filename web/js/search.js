$(function(){
    $("#txt-search").click( function() {
        $(this).animate({
            width:"100px"
        }, 200);
        $(this).addClass('search-focus');
    } ).blur( function() {
        $(this).animate({
            width:0
        }, 200);
        $(this).removeClass('search-focus');
    } );
});