$(function(){
	function toggleSideMenu() {
		$('ul.sidebar li a.side-name')
			.unbind('click')
			.click(function(){
				$(this).parent().find('ul').slideToggle('medium', function(){
					$(this).parent().toggleClass('active');
				});
			});
	}
    toggleSideMenu();
});