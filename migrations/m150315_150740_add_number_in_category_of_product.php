<?php

use app\models\CategoryOfProduct;
use yii\db\Schema;
use yii\db\Migration;

class m150315_150740_add_number_in_category_of_product extends Migration
{
    public function up()
    {
	    $this->addColumn(CategoryOfProduct::tableName(), 'number', Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0 COMMENT "Порядковый номер"');
    }

    public function down()
    {
        echo "m150315_150740_add_number_in_category_of_product cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
