<?php

use yii\db\Schema;
use yii\db\Migration;

class m150313_155723_create_category_of_product_table extends Migration
{
    public function up()
    {
	    $this->createTable('category_of_product', [
		    'id' => Schema::TYPE_PK . ' COMMENT "ID"',
		    'name' => Schema::TYPE_STRING . '(50) NOT NULL COMMENT "Категория"',
		    'parent_id' => Schema::TYPE_INTEGER . ' DEFAULT NULL COMMENT "ID родителя"',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('parent_id_FK_category_of_product', 'category_of_product', 'parent_id', 'category_of_product', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150313_155723_create_category_of_product_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
