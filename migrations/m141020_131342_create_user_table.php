<?php

use yii\db\Schema;
use yii\db\Migration;

class m141020_131342_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => Schema::TYPE_PK,
            'nick' => Schema::TYPE_STRING . '(50) NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('user_tbl_id_idx', 'user', 'id', true);
    }

    public function down()
    {
        echo "m141020_131342_create_user_table cannot be reverted.\n";

        return false;
    }
}
