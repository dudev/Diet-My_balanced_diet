<?php

use yii\db\Schema;
use yii\db\Migration;

class m150210_201614_create_product_table extends Migration
{
    public function up()
    {
	    $this->createTable('product', [
		    'id' => Schema::TYPE_PK . ' COMMENT "ID"',
		    'name' => Schema::TYPE_STRING . '(50) NOT NULL COMMENT "Название"',
		    'description' => Schema::TYPE_TEXT . ' COMMENT "Описание"',
		    'protein' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Белки"',
		    'fat' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Жиры"',
		    'carbohydrate' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Углеводы"',
		    'calorie' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Калорийность"',
		    'dietary_fiber' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Пищевые волокна"',
		    'cholesterol' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Холестерин"',
		    'water' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Процент воды"',
		    'ca' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Кальций. Ca, мг"',
		    'fe' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Железо. Fe, мг"',
		    'na' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Натрий. Na, мг"',
		    'k' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Калий. K, мг"',
		    'mg' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Магний. Mg, мг"',
		    'p' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "P, мг"',
		    'co' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Co, мг"',
		    'cr' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Cr, мг"',
		    'cu' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Cu, мг"',
		    'mn' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Mn, мг"',
		    's' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "S, мг"',
		    'si' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Si, мг"',
		    'v' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "V, мг"',
		    'zn' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Zn, мг"',
		    'mo' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Mo, мг"',
		    'f' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "F, мг"',
		    'i' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "I, мг"',
		    'se' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Se, мг"',
		    'b' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "B, мг"',
		    'cl' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Cl, мг"',
		    'a' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "A, мг"',
		    'b_car' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "B-car, мг"',
		    'b1' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "B1, мг"',
		    'b2' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "B2, мг"',
		    'b4' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "B4, мг"',
		    'b5' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "B5, мг"',
		    'b6' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "B6, мг"',
		    'b7' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "B7, мг"',
		    'b9' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "B9, мг"',
		    'b12' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "B12, мг"',
		    'c' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "C, мг"',
		    'd' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "D, мг"',
		    'e' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "E, мг"',
		    'pp' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "PP, мг"',
		    'k_phylloquinone' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "K (филлохинон), мг"',
		    'tryptophan' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Триптофан, мг"',
		    'lysine' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Лизин, мг"',
		    'methionine' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Метионин, мг"',
		    'threonine' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Треонин, мг"',
		    'leucine' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Лейцин, мг"',
		    'isoleucine' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Изолейцин, мг"',
		    'phenylalanine' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Фенилаланин, мг"',
		    'valine' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0 COMMENT "Валин, мг"',
		    'owner_id' => Schema::TYPE_INTEGER . ' DEFAULT NULL COMMENT "Владелец. NULL - системный"',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('owner_id_FK_product', 'product', 'owner_id', 'user', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        echo "m150210_201614_create_product_table cannot be reverted.\n";

        return false;
    }
}
