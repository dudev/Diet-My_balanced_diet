<?php

use yii\db\Schema;
use yii\db\Migration;

class m150313_155738_add_category_id_in_product_table extends Migration
{
    public function up()
    {
	    $this->addColumn('product', 'category_id', Schema::TYPE_INTEGER . ' COMMENT "Категория"');
	    $this->addForeignKey('category_id_FK_product', 'product', 'category_id', 'category_of_product', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        echo "m150313_155738_add_category_id_in_product_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
