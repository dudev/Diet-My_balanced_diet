<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\widgets;


use yii\base\Widget;

class MaxWidthBlock extends Widget {
	public $maxWidth;
	public static function begin($config = []) {
		/* @var $widget MaxWidthBlock */
		$widget = parent::begin($config);
		echo $widget->startRun();
	}
	public function startRun() {
		if($this->maxWidth) {
			return '<div style="max-width: ' . $this->maxWidth . 'px; margin: 0 auto;">';
		}
		return '';
	}
	public function run() {
		if($this->maxWidth) {
			return '</div>';
		}
		return '';
	}
} 