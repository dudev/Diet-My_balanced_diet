<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
use yii\helpers\Html;

/* @var $model array */
?>
<div class="row<?= $model['picture'] ? ' row-article' : '' ?>">
	<a href="<?= Yii::$app->urlManager->createUrl(['article/view', 'id' => $model['id']]) ?>" class="h2"><?= Html::encode($model['title']) ?></a>
	<? if($model['picture'] && !empty($model['image_links'])) { ?>
		<a href="<?= Yii::$app->urlManager->createUrl(['article/view', 'id' => $model['id']]) ?>" class="small_image_article">
			<img src="<?= !empty($model['image_links']['small']) ? $model['image_links']['small'] : '' ?>">
		</a>
	<? } ?>
	<?= $model['description'] ?>
</div>
