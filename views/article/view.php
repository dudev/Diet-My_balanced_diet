<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

/* @var $this \yii\web\View */
/* @var $data array */
$this->title = $data['title'];

$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['article/list']];
$this->params['breadcrumbs'][] = ['label' => $data['tags'][0], 'url' => ['article/list', 'tag' => $data['tags'][0]]];
$this->params['breadcrumbs'][] = $data['title'];

$this->params['width'] = 900;

$this->registerMetaTag([
	'name' => 'keywords',
	'content' => $data['keywords'],
]);
$this->registerMetaTag([
	'name' => 'description',
	'content' => $data['description'],
]);
?>
<div class="row">
	<?=
	($data['pic_in_post'] && $data['picture'])
		? '<img src="' . $data['image_links']['small'] . '" class="big_image_article">'
		: ''
	?>
	<?= $data['text'] ?>


	<script type="text/javascript">(function(w,doc) {
			if (!w.__utlWdgt ) {
				w.__utlWdgt = true;
				var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
				s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
				s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
				var h=d[g]('body')[0];
				h.appendChild(s);
			}})(window,document);
	</script>
	<div data-share-size="40" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1356620" data-mode="share" data-background-color="#ffffff" data-share-shape="round" data-share-counter-size="12" data-icon-color="#ffffff" data-text-color="#000000" data-buttons-color="#ffffff" data-counter-background-color="#ffffff" data-share-counter-type="disable" data-orientation="horizontal" data-following-enable="false" data-sn-ids="fb.vk.tw.ok.gp." data-selection-enable="true" data-exclude-show-more="false" data-share-style="1" data-counter-background-alpha="1.0" data-top-button="false" class="uptolike-buttons" ></div>
</div>
