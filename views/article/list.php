<?php

use app\assets\ArticleAsset;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $tag string|null */

if($tag) {
	$this->title = $tag;
	$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['article/list']];
	$this->params['breadcrumbs'][] = $tag;
} else {
	$this->title = 'Статьи';
	$this->params['breadcrumbs'][] = $this->title;
}

$this->params['width'] = 900;

ArticleAsset::register($this);
?>
<div class="menu-index">
	<?=
	ListView::widget([
		'dataProvider' => $dataProvider,
		'itemView' => '_article',
		'layout' => '{items}{pager}',
	])
	?>

</div>
