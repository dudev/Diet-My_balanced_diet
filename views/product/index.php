<?php

use app\assets\ProductAsset;
use app\models\CategoryOfProduct;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $category null|int */

if($category && $category = CategoryOfProduct::findOne($category)) {
	/* @var $category CategoryOfProduct */
	$this->title = $category->name;
	$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['product/index']];
} else {
	$this->title = 'Продукты';
}
$this->params['breadcrumbs'][] = $this->title;

$this->params['width'] = 900;

ProductAsset::register($this);
?>
<div class="product-index">
	<?=
	ListView::widget([
		'dataProvider' => $dataProvider,
		'itemView' => '_product',
		'layout' => '{items}{pager}',
	])
	?>
</div>
