<?php

use app\assets\ProductAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];

if($model->category) {
	$this->params['breadcrumbs'][] = [
		'label' => $model->category->name,
		'url' => [
			'product/index',
			'category' => $model->category-> id,
		]
	];
}
$this->params['breadcrumbs'][] = $this->title;

$this->params['width'] = 900;

ProductAsset::register($this);
?>
<div class="row">
	<? if(isset($model::getImageLinks($model, 'small')['small'])): ?>
		<img src="<?= $model::getImageLinks($model, 'small')['small'] ?>" class="product_img">
	<? endif; ?>
	<span><?= $model->description ?></span>
	<h2>Нутриенты на 100 г</h2>
	<div class="nutrients left">
		<table>
			<tr>
				<th>Белки</th>
				<td><span><?= $model->protein ?> г<!--div class="normal">Суточная норма:<br>100 г</div--></span></td>
			</tr>
			<tr>
				<th>Жиры</th>
				<td><span><?= $model->fat ?> г</span></td>
			</tr>
			<tr>
				<th>Углеводы</th>
				<td><span><?= $model->carbohydrate ?> г</span></td>
			</tr>
			<tr>
				<th>Калорийность</th>
				<td><span><?= $model->calorie ?> ккал</span></td>
			</tr>
			<tr>
				<th>Пищевые волокна</th>
				<td><span><?= $model->dietary_fiber ?> г</span></td>
			</tr>
			<tr>
				<th>Холестерин</th>
				<td><span><?= $model->cholesterol ?> г</span></td>
			</tr>
			<tr>
				<th>Вода</th>
				<td><span><?= $model->water ?> %</span></td>
			</tr>
			<tr>
				<th>Кальций (Ca)</th>
				<td><span><?= $model->ca ?> мг</span></td>
			</tr>
			<tr>
				<th>Железо (Fe)</th>
				<td><span><?= $model->fe ?> мг</span></td>
			</tr>
			<tr>
				<th>Натрий (Na)</th>
				<td><span><?= $model->na ?> мг</span></td>
			</tr>
			<tr>
				<th>Калий (K)</th>
				<td><span><?= $model->k ?> мг</span></td>
			</tr>
			<tr>
				<th>Магний (Mg)</th>
				<td><span><?= $model->mg ?> мг</span></td>
			</tr>
			<tr>
				<th>Фосфор (P)</th>
				<td><span><?= $model->p ?> мг</span></td>
			</tr>
			<tr>
				<th>Кобальт (Co)</th>
				<td><span><?= $model->co ?> мг</span></td>
			</tr>
			<tr>
				<th>Хром (Cr)</th>
				<td><span><?= $model->cr ?> мг</span></td>
			</tr>
			<tr>
				<th>Медь (Cu)</th>
				<td><span><?= $model->cu ?> мг</span></td>
			</tr>
			<tr>
				<th>Марганец (Mn)</th>
				<td><span><?= $model->mn ?> мг</span></td>
			</tr>
			<tr>
				<th>Сера (S)</th>
				<td><span><?= $model->s ?> мг</span></td>
			</tr>
			<tr>
				<th>Кремний (Si)</th>
				<td><span><?= $model->si ?> мг</span></td>
			</tr>
			<tr>
				<th>Ванадий (V)</th>
				<td><span><?= $model->v ?> мг</span></td>
			</tr>
			<tr>
				<th>Цинк (Zn)</th>
				<td><span><?= $model->zn ?> мг</span></td>
			</tr>
			<tr>
				<th>Молибден (Mo)</th>
				<td><span><?= $model->mo ?> мг</span></td>
			</tr>
			<tr>
				<th>Фтор (F)</th>
				<td><span><?= $model->f ?> мг</span></td>
			</tr>
			<tr>
				<th>Йод (I)</th>
				<td><span><?= $model->i ?> мг</span></td>
			</tr>
			<tr>
				<th>Селен (Se)</th>
				<td><span><?= $model->se ?> мг</span></td>
			</tr>
		</table>
	</div>
	<div class="nutrients right">
		<table>
			<tr>
				<th>Бор (B)</th>
				<td><span><?= $model->b ?> мг</span></td>
			</tr>
			<tr>
				<th>Хлор (Cl)</th>
				<td><span><?= $model->cl ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин А</th>
				<td><span><?= $model->a ?> мг</span></td>
			</tr>
			<tr>
				<th>Бета-каротин</th>
				<td><span><?= $model->b_car ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин B1</th>
				<td><span><?= $model->b1 ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин B2</th>
				<td><span><?= $model->b2 ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин B4</th>
				<td><span><?= $model->b4 ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин B5</th>
				<td><span><?= $model->b5 ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин B6</th>
				<td><span><?= $model->b6 ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин B7</th>
				<td><span><?= $model->b7 ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин B9</th>
				<td><span><?= $model->b9 ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин B12</th>
				<td><span><?= $model->b12 ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин C</th>
				<td><span><?= $model->c ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин D</th>
				<td><span><?= $model->d ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин E</th>
				<td><span><?= $model->e ?> мг</span></td>
			</tr>
			<tr>
				<th>Витамин PP</th>
				<td><span><?= $model->pp ?> мг</span></td>
			</tr>
			<tr>
				<th title="Витамин K (филлохинон)">Витамин K</th>
				<td><span><?= $model->k_phylloquinone ?> мг</span></td>
			</tr>
			<tr>
				<th>Триптофан</th>
				<td><span><?= $model->tryptophan ?> мг</span></td>
			</tr>
			<tr>
				<th>Лизин</th>
				<td><span><?= $model->lysine ?> мг</span></td>
			</tr>
			<tr>
				<th>Метионин</th>
				<td><span><?= $model->methionine ?> мг</span></td>
			</tr>
			<tr>
				<th>Треонин</th>
				<td><span><?= $model->threonine ?> мг</span></td>
			</tr>
			<tr>
				<th>Лейцин</th>
				<td><span><?= $model->leucine ?> мг</span></td>
			</tr>
			<tr>
				<th>Изолейцин</th>
				<td><span><?= $model->isoleucine ?> мг</span></td>
			</tr>
			<tr>
				<th>Фенилаланин</th>
				<td><span><?= $model->phenylalanine ?> мг</span></td>
			</tr>
			<tr>
				<th>Валин</th>
				<td><span><?= $model->valine ?> мг</span></td>
			</tr>
		</table>
	</div>
	<script type="text/javascript">(function(w,doc) {
			if (!w.__utlWdgt ) {
				w.__utlWdgt = true;
				var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
				s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
				s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
				var h=d[g]('body')[0];
				h.appendChild(s);
			}})(window,document);
	</script>
	<div data-share-size="40" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1356620" data-mode="share" data-background-color="#ffffff" data-share-shape="round" data-share-counter-size="12" data-icon-color="#ffffff" data-text-color="#000000" data-buttons-color="#ffffff" data-counter-background-color="#ffffff" data-share-counter-type="disable" data-orientation="horizontal" data-following-enable="false" data-sn-ids="fb.vk.tw.ok.gp." data-selection-enable="true" data-exclude-show-more="false" data-share-style="1" data-counter-background-alpha="1.0" data-top-button="false" class="uptolike-buttons" ></div>
</div>
