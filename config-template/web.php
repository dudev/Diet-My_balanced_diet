<?php

//добавил общие классы
Yii::setAlias('@general', realpath(__DIR__ . '/../../general'));
Yii::setAlias('@general/ext', realpath(__DIR__ . '/../../general/extensions'));

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'diet',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['log'],
	'defaultRoute' => 'article/list',
    'components' => [
	    'assetManager' => [
		    'class' => 'yii\web\AssetManager',
		    'appendTimestamp' => true,
	    ],
        'request' => [
            'cookieValidationKey' => 'qym6SFk47z39-RFHUQeEZu1x7ll3g8En',
	        'csrfParam' => '__check__',
        ],
	    'security' => [
		    'passwordHashStrategy' => 'password_hash',
	    ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
	        'absoluteAuthTimeout' => 300,
	        'identityCookie' => ['name' => '_token', 'httpOnly' => true],
        ],
	    'urlManager' => [
		    'enablePrettyUrl' => true,
		    'showScriptName' => false,
		    'rules' => [
			    'article' => 'article/list',
			    'article/<id:\d+>' => 'article/view',
			    'product' => 'product/index',
			    'product/<id:\d+>' => 'product/view',
			    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
		    ]
	    ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

/*if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}*/

return $config;
