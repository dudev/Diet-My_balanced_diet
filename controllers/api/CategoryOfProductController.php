<?php

namespace app\controllers\api;


use app\models\CategoryOfProduct;
use general\controllers\api\Controller;
use yii\base\Exception;
use yii\db\Expression;
use yii\db\Query;

class CategoryOfProductController extends Controller {
    public function actionCreate() {
	    $model = new CategoryOfProduct();

	    if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
		    $transaction = \Yii::$app->db->beginTransaction();
		    try {
			    $this->moveDown($model);
			    $model->save(false);
			    $transaction->commit();
			    return $this->sendSuccess([
				    'category' => [
					    'id' => $model->id,
					    'name' => $model->name,
					    'parent_id' => $model->parent_id,
					    'number' => $model->number,
				    ]
			    ]);
		    } catch (Exception $e) {
			    $transaction->rollBack();
			    return $this->sendError(self::ERROR_DB);
		    }
	    } else {
		    $errors = $this->getErrorCodes([
			    'name' => self::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NAME,
			    'parent_id' => self::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_PARENT_ID,
			    'number' => self::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NUMBER,
		    ], $model);
	    }

	    if(empty($errors)) {
		    $errors = self::ERROR_UNKNOWN;
	    }
	    return $this->sendError($errors);
    }
    public function actionDelete($id) {
	    /* @var $model CategoryOfProduct */
	    if($model = CategoryOfProduct::findOne($id)) {
		    $transaction = \Yii::$app->db->beginTransaction();
		    try {
			    $this->moveUp($model);
			    $model->delete();
			    $transaction->commit();
			    return $this->sendSuccess([]);
		    } catch(Exception $e) {
			    $transaction->rollBack();
			    return $this->sendError(self::ERROR_DB);
		    }
	    } else {
		    return $this->sendError(self::ERROR_NO_CATEGORY_OF_ARTICLE);
	    }
    }
    public function actionIndex() {
	    $parent_id = \Yii::$app->request->get('parent_id');
        $model = CategoryOfProduct::find()
	        ->select(['id', 'name', 'parent_id', 'number']);
	    if(!is_null($parent_id)) {
		    $model->where([
			    'parent_id' => $parent_id
		    ]);
	    }
	    if ($sort = \Yii::$app->request->get('sort')) {
		    if(strncmp($sort, '-', 1) === 0) {
			    $model->orderBy([substr($sort, 1) => SORT_DESC]);
		    } else {
			    $model->orderBy([$sort => SORT_ASC]);
		    }
	    }
	    $model = $model->asArray()
            ->all();
	    return $this->sendSuccess(['categories' => $model]);
    }
    public function actionUpdate($id) {
	    /* @var $model CategoryOfProduct */
	    $model = CategoryOfProduct::findOne($id);

	    if(\Yii::$app->request->isPost) {
		    if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
			    $transaction = \Yii::$app->db->beginTransaction();
			    try {
				    if ($model->getOldAttribute('number') != $model->number) {
					    $this->moveUp($model);
					    $this->moveDown($model);
				    }
				    $model->save(false);
				    $transaction->commit();
			    } catch (Exception $e) {
				    $transaction->rollBack();
				    return $this->sendError(self::ERROR_DB);
			    }
		    } else {
			    $errors = $this->getErrorCodes([
				    'name' => self::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NAME,
				    'parent_id' => self::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_PARENT_ID,
				    'number' => self::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NUMBER,
			    ], $model);
			    return $this->sendError($errors);
		    }
	    }

	    return $this->sendSuccess([
		    'category' => [
			    'id' => $model->id,
			    'name' => $model->name,
			    'parent_id' => $model->parent_id,
			    'number' => $model->number,
		    ]
	    ]);
    }
	public function actionMaxNumber($parent_id) {
		$max = CategoryOfProduct::find()
			->where([
				'parent_id' => $parent_id == 0 ? null : $parent_id,
			])
			->max('number');
		return $this->sendSuccess([
			'max_number' => (int)$max,
		]);
	}
	/**
	 * Поднимает пункты меню
	 * @param CategoryOfProduct $model
	 * @throws \yii\db\Exception
	 */
	private function moveUp(CategoryOfProduct $model) {
		(new Query())
			->createCommand()
			->update(
				CategoryOfProduct::tableName(),
				[
					'number' => new Expression('number - 1')
				],
				[
					'and',
					['>', 'number', $model->number],
					['parent_id' => $model->parent_id],
				]
			)
			->execute();
	}
	/**
	 * Упускает пункты меню
	 * @param CategoryOfProduct $model
	 * @throws \yii\db\Exception
	 */
	private function moveDown(CategoryOfProduct $model) {
		(new Query())
			->createCommand()
			->update(
				CategoryOfProduct::tableName(),
				[
					'number' => new Expression('number + 1')
				],
				[
					'and',
					['>=', 'number', $model->number],
					['parent_id' => $model->parent_id],
				]
			)
			->execute();
	}
}
