<?php

namespace app\controllers\api;

use general\controllers\api\Controller;
use Yii;
use app\models\Product;
use app\models\api\search\ProductSearch;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller {
	/**
	 * @param int $page
	 * @return array
	 */
    public function actionIndex($page = 0) {
	    if($page > 100) {
		    return $this->sendError(self::ERROR_TOO_BIG_PAGE_NUMBER);
	    }

	    $data = (new ProductSearch())->search(\Yii::$app->request->get(), $page);

	    if(is_int($data)) {
		    return $this->sendError($data);
	    }

	    if($data === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    return $this->sendSuccess($data);
    }
    /**
     * Creates a new Product model.
     * @return array
     */
    public function actionCreate() {
	    if(\Yii::$app->request->isGet) {
		    return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
	    }
	    $model = new Product();

	    if(!$model->load(\Yii::$app->request->post())) {
		    return $this->sendError(self::ERROR_NO_DATA);
	    }
	    $model->image = UploadedFile::getInstance($model, 'image');

	    if($model->validate()) {
		    if(!$model->save(false)) {
			    return $this->sendError(self::ERROR_DB);
		    }
		    $model->saveImage();

		    return $this->sendSuccess([
			    'product' => array_merge(
				    $model->getAttributes(null, ['updated_at', 'created_at']),
				    ['image_links' => Product::getImageLinks($model)]
			    )
		    ]);
	    } else {
		    $errors = $this->getErrorCodes([
			    'name' => self::ERROR_ILLEGAL_PRODUCT_NAME,
			    'description' => self::ERROR_ILLEGAL_PRODUCT_DESCRIPTION,
			    'protein' => self::ERROR_ILLEGAL_PRODUCT_PROTEIN,
			    'fat' => self::ERROR_ILLEGAL_PRODUCT_FAT,
			    'carbohydrate' => self::ERROR_ILLEGAL_PRODUCT_CARBOHYDRATE,
			    'calorie' => self::ERROR_ILLEGAL_PRODUCT_CALORIE,
			    'dietary_fiber' => self::ERROR_ILLEGAL_PRODUCT_DIETARY_FIBER,
			    'cholesterol' => self::ERROR_ILLEGAL_PRODUCT_CHOLESTEROL,
			    'water' => self::ERROR_ILLEGAL_PRODUCT_WATER,
			    'ca' => self::ERROR_ILLEGAL_PRODUCT_CA,
			    'fe' => self::ERROR_ILLEGAL_PRODUCT_FE,
			    'na' => self::ERROR_ILLEGAL_PRODUCT_NA,
			    'k' => self::ERROR_ILLEGAL_PRODUCT_K,
			    'mg' => self::ERROR_ILLEGAL_PRODUCT_MG,
			    'p' => self::ERROR_ILLEGAL_PRODUCT_P,
			    'co' => self::ERROR_ILLEGAL_PRODUCT_CO,
			    'cr' => self::ERROR_ILLEGAL_PRODUCT_CR,
			    'cu' => self::ERROR_ILLEGAL_PRODUCT_CU,
			    'mn' => self::ERROR_ILLEGAL_PRODUCT_MN,
			    's' => self::ERROR_ILLEGAL_PRODUCT_S,
			    'si' => self::ERROR_ILLEGAL_PRODUCT_SI,
			    'v' => self::ERROR_ILLEGAL_PRODUCT_V,
			    'zn' => self::ERROR_ILLEGAL_PRODUCT_ZN,
			    'mo' => self::ERROR_ILLEGAL_PRODUCT_MO,
			    'f' => self::ERROR_ILLEGAL_PRODUCT_F,
			    'i' => self::ERROR_ILLEGAL_PRODUCT_I,
			    'se' => self::ERROR_ILLEGAL_PRODUCT_SE,
			    'b' => self::ERROR_ILLEGAL_PRODUCT_B,
			    'cl' => self::ERROR_ILLEGAL_PRODUCT_CL,
			    'a' => self::ERROR_ILLEGAL_PRODUCT_A,
			    'b_car' => self::ERROR_ILLEGAL_PRODUCT_B_CAR,
			    'b1' => self::ERROR_ILLEGAL_PRODUCT_B1,
			    'b2' => self::ERROR_ILLEGAL_PRODUCT_B2,
			    'b4' => self::ERROR_ILLEGAL_PRODUCT_B4,
			    'b5' => self::ERROR_ILLEGAL_PRODUCT_B5,
			    'b6' => self::ERROR_ILLEGAL_PRODUCT_B6,
			    'b7' => self::ERROR_ILLEGAL_PRODUCT_B7,
			    'b9' => self::ERROR_ILLEGAL_PRODUCT_B9,
			    'b12' => self::ERROR_ILLEGAL_PRODUCT_B12,
			    'c' => self::ERROR_ILLEGAL_PRODUCT_C,
			    'd' => self::ERROR_ILLEGAL_PRODUCT_D,
			    'e' => self::ERROR_ILLEGAL_PRODUCT_E,
			    'pp' => self::ERROR_ILLEGAL_PRODUCT_PP,
			    'k_phylloquinone' => self::ERROR_ILLEGAL_PRODUCT_K_PHYLLOQUINONE,
			    'tryptophan' => self::ERROR_ILLEGAL_PRODUCT_TRYPTOPHAN,
			    'lysine' => self::ERROR_ILLEGAL_PRODUCT_LYSINE,
			    'methionine' => self::ERROR_ILLEGAL_PRODUCT_METHIONINE,
			    'threonine' => self::ERROR_ILLEGAL_PRODUCT_THREONINE,
			    'leucine' => self::ERROR_ILLEGAL_PRODUCT_LEUCINE,
			    'isoleucine' => self::ERROR_ILLEGAL_PRODUCT_ISOLEUCINE,
			    'phenylalanine' => self::ERROR_ILLEGAL_PRODUCT_PHENYLALANINE,
			    'valine' => self::ERROR_ILLEGAL_PRODUCT_VALINE,
			    'owner_id' => self::ERROR_ILLEGAL_PRODUCT_OWNER_ID,
			    'copy_from' => self::ERROR_ILLEGAL_PRODUCT_COPY_FROM,
			    'category_id' => self::ERROR_ILLEGAL_PRODUCT_CATEGORY_ID,
			    'image' => self::ERROR_ILLEGAL_PRODUCT_IMAGE,
		    ], $model);
	    }

	    if(!isset($errors)) {
		    $errors = self::ERROR_UNKNOWN;
	    }
	    return $this->sendError($errors);
    }
    /**
     * Updates an existing Product model.
     * @param integer $id
     * @return array
     */
    public function actionUpdate($id) {
	    /* @var Product $model */
	    $model = Product::findOne($id);
	    if(!$model) {
		    return $this->sendError(self::ERROR_NO_PRODUCT);
	    }
	    if(\Yii::$app->request->isPost) {
		    if (!$model->load(\Yii::$app->request->post())) {
			    return $this->sendError(self::ERROR_NO_DATA);
		    }
		    $model->image = UploadedFile::getInstance($model, 'image');
		    if ($model->validate()) {
			    if (!$model->save(false)) {
				    return $this->sendError(self::ERROR_DB);
			    }
			    $model->saveImage();
		    } else {
			    $errors = $this->getErrorCodes([
				    'name' => self::ERROR_ILLEGAL_PRODUCT_NAME,
				    'description' => self::ERROR_ILLEGAL_PRODUCT_DESCRIPTION,
				    'protein' => self::ERROR_ILLEGAL_PRODUCT_PROTEIN,
				    'fat' => self::ERROR_ILLEGAL_PRODUCT_FAT,
				    'carbohydrate' => self::ERROR_ILLEGAL_PRODUCT_CARBOHYDRATE,
				    'calorie' => self::ERROR_ILLEGAL_PRODUCT_CALORIE,
				    'dietary_fiber' => self::ERROR_ILLEGAL_PRODUCT_DIETARY_FIBER,
				    'cholesterol' => self::ERROR_ILLEGAL_PRODUCT_CHOLESTEROL,
				    'water' => self::ERROR_ILLEGAL_PRODUCT_WATER,
				    'ca' => self::ERROR_ILLEGAL_PRODUCT_CA,
				    'fe' => self::ERROR_ILLEGAL_PRODUCT_FE,
				    'na' => self::ERROR_ILLEGAL_PRODUCT_NA,
				    'k' => self::ERROR_ILLEGAL_PRODUCT_K,
				    'mg' => self::ERROR_ILLEGAL_PRODUCT_MG,
				    'p' => self::ERROR_ILLEGAL_PRODUCT_P,
				    'co' => self::ERROR_ILLEGAL_PRODUCT_CO,
				    'cr' => self::ERROR_ILLEGAL_PRODUCT_CR,
				    'cu' => self::ERROR_ILLEGAL_PRODUCT_CU,
				    'mn' => self::ERROR_ILLEGAL_PRODUCT_MN,
				    's' => self::ERROR_ILLEGAL_PRODUCT_S,
				    'si' => self::ERROR_ILLEGAL_PRODUCT_SI,
				    'v' => self::ERROR_ILLEGAL_PRODUCT_V,
				    'zn' => self::ERROR_ILLEGAL_PRODUCT_ZN,
				    'mo' => self::ERROR_ILLEGAL_PRODUCT_MO,
				    'f' => self::ERROR_ILLEGAL_PRODUCT_F,
				    'i' => self::ERROR_ILLEGAL_PRODUCT_I,
				    'se' => self::ERROR_ILLEGAL_PRODUCT_SE,
				    'b' => self::ERROR_ILLEGAL_PRODUCT_B,
				    'cl' => self::ERROR_ILLEGAL_PRODUCT_CL,
				    'a' => self::ERROR_ILLEGAL_PRODUCT_A,
				    'b_car' => self::ERROR_ILLEGAL_PRODUCT_B_CAR,
				    'b1' => self::ERROR_ILLEGAL_PRODUCT_B1,
				    'b2' => self::ERROR_ILLEGAL_PRODUCT_B2,
				    'b4' => self::ERROR_ILLEGAL_PRODUCT_B4,
				    'b5' => self::ERROR_ILLEGAL_PRODUCT_B5,
				    'b6' => self::ERROR_ILLEGAL_PRODUCT_B6,
				    'b7' => self::ERROR_ILLEGAL_PRODUCT_B7,
				    'b9' => self::ERROR_ILLEGAL_PRODUCT_B9,
				    'b12' => self::ERROR_ILLEGAL_PRODUCT_B12,
				    'c' => self::ERROR_ILLEGAL_PRODUCT_C,
				    'd' => self::ERROR_ILLEGAL_PRODUCT_D,
				    'e' => self::ERROR_ILLEGAL_PRODUCT_E,
				    'pp' => self::ERROR_ILLEGAL_PRODUCT_PP,
				    'k_phylloquinone' => self::ERROR_ILLEGAL_PRODUCT_K_PHYLLOQUINONE,
				    'tryptophan' => self::ERROR_ILLEGAL_PRODUCT_TRYPTOPHAN,
				    'lysine' => self::ERROR_ILLEGAL_PRODUCT_LYSINE,
				    'methionine' => self::ERROR_ILLEGAL_PRODUCT_METHIONINE,
				    'threonine' => self::ERROR_ILLEGAL_PRODUCT_THREONINE,
				    'leucine' => self::ERROR_ILLEGAL_PRODUCT_LEUCINE,
				    'isoleucine' => self::ERROR_ILLEGAL_PRODUCT_ISOLEUCINE,
				    'phenylalanine' => self::ERROR_ILLEGAL_PRODUCT_PHENYLALANINE,
				    'valine' => self::ERROR_ILLEGAL_PRODUCT_VALINE,
				    'owner_id' => self::ERROR_ILLEGAL_PRODUCT_OWNER_ID,
				    'copy_from' => self::ERROR_ILLEGAL_PRODUCT_COPY_FROM,
				    'category_id' => self::ERROR_ILLEGAL_PRODUCT_CATEGORY_ID,
				    'image' => self::ERROR_ILLEGAL_PRODUCT_IMAGE,
			    ], $model);
			    return $this->sendError($errors);
		    }
	    }

	    return $this->sendSuccess([
		    'product' => array_merge(
			    $model->getAttributes(null, ['updated_at', 'created_at']),
			    ['image_links' => Product::getImageLinks($model)]
		    )
	    ]);
    }
    /**
     * Deletes an existing Product model.
     * @param integer $id
     * @return array
     */
    public function actionDelete($id) {
	    /* @var Product $model */
	    if($model = Product::findOne($id)) {
		    if($model->delete()) {
			    return $this->sendSuccess([]);
		    } else {
			    return $this->sendError(self::ERROR_DB);
		    }
	    } else {
		    return $this->sendError(self::ERROR_NO_PRODUCT);
	    }
    }
}
