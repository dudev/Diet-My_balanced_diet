<?php

namespace app\controllers;

use app\extensions\Controller;
use Yii;
use app\models\Product;
use app\models\search\ProductSearch;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

	/**
	 * Lists all Product models.
	 * @param number|null $category
	 * @return mixed
	 */
    public function actionIndex($category = null) {
	    $query = Product::find()
		    ->where(['NOT', ['description' => null]])
		    ->andWhere(['!=', 'description', '']);

	    if($category) {
		    $query
			    ->andWhere(is_null($category) ? [] : ['category_id' => $category]);
	    }

	    $dataProvider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
			    'pageSize' => 15,
		    ],
	    ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
	        'category' => $category,
        ]);
    }

	/**
	 * Displays a single Product model.
	 * @param integer $id
	 * @throws NotFoundHttpException
	 * @return mixed
	 */
    public function actionView($id) {
	    $model = Product::find()
		    ->where(['id' => $id])
		    ->andWhere(['owner_id' => null])
		    ->one();
	    if (!$model) {
		    throw new NotFoundHttpException('The requested page does not exist.');
	    }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = Product::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
	    Product::findOne($id)->delete();

        return $this->redirect(['index']);
    }*/

	protected function findModel($id)
	{
		if (($model = Product::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
