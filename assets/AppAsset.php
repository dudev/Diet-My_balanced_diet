<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main.css',
	    'css/fonts.css',
	    'css/column2.css',
    ];
    public $js = [
	    'js/search.js',
	    'js/side-menu.js',
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
    ];
}
